# README #

### What is this repository for? ###

* Create & test advanced queries for [q_](https://bitbucket.org/tdhasenpflug/q_)

### How do I get set up? ###

* Install [q_](https://bitbucket.org/tdhasenpflug/q_)
* Install in /wp-content/plugins/
 
### Contribution guidelines ###

* Don't be evil.

### Who do I talk to? ###

* [click to email me](mailto:tdhasenpflug@gmail.com)