<?php
/*
 * Plugin Name: q_live
 * Plugin URI: http://taylorhasenpflug.com/q_live/
 * Description: Requires q_
 * Author: Taylor Hasenpflug
 * Version: 0.0.1
 */

if (!class_exists('q__')) { return; }

class q__live {
    public function __construct()
    {
        add_action('admin_menu', array($this, 'admin_menu'));
        add_action('wp_ajax_q__live', array($this, 'ajax'));
    }
    public function admin_menu()
    {
        add_management_page('q__live', 'q__live', 'manage_options', 'q__live', array($this, 'page'));
    }
    public function page()
    {
        ?><div class='wrap q'><h2>q__live live querier</h2>
            <section id='q__live-form' class='qbox'>
                <form>
                    <p><label for='search'>Search: </label><input type='text' id='search' name='search' class='widefat' /></p>
                    <p><label for='name'>Name: </label><input type='text' id='name' name='name' class='widefat' /></p>
                    
                    <p><label for='author'>Author: </label><input type='text' id='author' name='author' class='widefat' /></p>
                    <p><label for='category'>Category: </label><input type='text' id='category' name='category' class='widefat' /></p>
                    <p><label for='tag'>Tag: </label><input type='text' id='tag' name='tag' class='widefat' /></p>
                    
                    <p><label for='count'>Count: </label><input type='number' id='parent' name='count' min='1' step='1' class='widefat' /></p>
                    
                    <p><label for='type'>Type: </label><select id='type' name='type' class='widefat'><option value=''>Any</option><?php
                        $types = get_post_types();
                        foreach($types as $t) {
                        ?><option value='<?php esc_attr_e($t); ?>'><?php esc_attr_e($t); ?></option><?php
                        }
                    ?></select></p>
                    
                    <p><label for='status'>Status: </label><select id='status' name='status' class='widefat'><option value=''>Any</option><option value='publish'>Publish</option><option value='pending'>Pending</option><option value='draft'>Draft</option><option value='auto-draft'>Auto-Draft</option><option value='future'>Future</option><option value='private'>Private</option><option value='inherit'>Inherit</option><option value='trash'>Trash</option></select></p>
                    
                    <p class='order'>Order:<label><input type='radio' name='order' value='ASC'> ASC</label><br /><label><input type='radio' name='order' value='DESC'> DESC</label></p>
                    
                    <p><label for='orderby'>Order By: </label><select id='orderby' name='orderby' class='widefat'><option value=''>None</option><option value='ID'>ID</option><option value='author'>Author</option><option value='title'>Title</option><option value='name'>Name</option><option value='type'>Type</option><option value='date'>Date</option><option value='modified'>Modified</option><option value='parent'>Parent</option><option value='rand'>Random</option><option value='comment_count'>Comment Count</option><option value='menu_order'>Menu Order</option></select></p>
                    
                    <div class='show-advanced'><span class='show-adv-btn qbtn' id='show-advanced'>Show Advanced Query Options<span class="dashicons dashicons-arrow-left-alt2"></span></span></div>
                    
                    <div class='advanced'>
                        <p><label for='id'>ID(s): </label><input type='text' id='id' name='id' class='widefat' /></p>
                        <p><label for='author_id'>Author ID(s): </label><input type='text' id='author_id' name='author_id' class='widefat' /></p>
                        <p><label for='category_id'>Category ID(s): </label><input type='text' id='category_id' name='category_id' class='widefat' /></p>
                        <p><label for='tag_id'>Tag ID(s): </label><input type='text' id='tag_id' name='tag_id' class='widefat' /></p>
                        <p><label for='parent'>Parent: </label><input type='text' id='parent' name='parent' class='widefat' /></p>
                        <p class='double'><label for='template'>Template</label><textarea id='template' name='template' class='widefat' rows='6' placeholder="<li>[q_link]</li>"></textarea></p>
                        <p class='double'><span>Insert Template Shortcode</span><br /><span class='add-qcode qbtn'>[q_title]</span><span class='add-qcode qbtn'>[q_link]</span><span class='add-qcode qbtn'>[q_url]</span><span class='add-qcode qbtn'>[q_date]</span><span class='add-qcode qbtn'>[q_content]</span><span class='add-qcode qbtn'>[q_excerpt]</span><span class='add-qcode qbtn'>[q_author]</span><span class='add-qcode qbtn'>[q_terms]</span><span class='add-qcode qbtn'>[q_status]</span><span class='add-qcode qbtn'>[q_type]</span><span class='add-qcode qbtn'>[q_id]</span><span class='add-qcode qbtn'>[q_image]</span></p>
                    </div>
                    
                </form>
            </section>
            <h3>Query Shortcode:</h3>
            <section id='q__live-querycode' class='qbox'><input type='text' value='[q_]' class='widefat' readonly /></section>
            <h3>Query Results: <span class='refresh'><label><input type='checkbox' id='q__live-refresh-toggle' checked /> Live Refresh</label><span id='q__live-refresh' class='qbtn'>Refresh Query</span></span></h3>
            <section id='q__live-results' class='qbox'><div class='show'><em>Nothing to show...yet</em></div></section>
        </div><style>
            .qbox {
                display:block;
                padding:16px;
                background:#fff;
                box-shadow:1px 2px 2px rgba(0,0,0,0.15);
                margin:16px 0;
            }
            .order:after,
            #q__live-form .advanced:after,
            .qbox:after {
                display:block;
                clear:both;
                content: '';
            }
            .q h3 {
                clear:both;
                margin:8px 0;
            }
            #q__live-form .advanced {
                display:none;
                padding:8px 0 0;
            }
            .q .refresh {
                font-size:14px;
                font-weight: 100;
                line-height: inherit;
                padding:0 0 0 8px;
            }
            .show-advanced {
                clear:both;
            }
            .qbtn {
                display:inline-block;
                border-radius:3px;
                text-decoration: none;
                border:1px solid rgba(0,0,0,0.25);
                background:rgba(0,0,0,0.05);
                padding:4px 8px;
                cursor: pointer;
            }
            .qbtn:hover {
                background:rgba(0,0,0,0.15);
                border-color:rgba(0,0,0,0.35);
            }
            .add-qcode {
                margin:4px;
            }
            #q__live-refresh {
                display: none;
                margin:0 0 0 8px;
            }
            .show-advanced .dashicons {
                margin:0 0 0 4px;
                font-size:inherit;
                line-height:inherit;
            }
            #q__live-form p {
                display:inline-block;
                padding:0 12px 8px 0;
                float:left;
                width:20%;
                min-width:200px;
                margin:0;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            #q__live-form p.double {
                width:40%;
            }
            #q__live-form p.triple {
                width:60%;
            }
            .order label {
                float:right;
                clear:both;
                width:50%;
            }
        </style><script>
            jQuery(document).ready(function($) {
                var qtimeout = null,
                    old_formdata = {},
                    old_template = '';
                function cobj(o1, o2) {
                    return (JSON.stringify(o1) === JSON.stringify(o2));
                }
                function q_query() {
                    var data = {
                        'action': 'q__live',
                        'query': {}
                    },
                        shortcode = 'q_',
                        formdata = $('#q__live-form > form').serializeArray();

                    $.each(formdata, function(i, obj) {
                        if (obj.value) {
                            if (obj.name === 'template') {
                                data.template = obj.value;
                            } else {
                                data.query[obj.name] = obj.value;
                                shortcode += ' ' + obj.name + '="' + obj.value + '"';
                            }
                        }
                    });
                    
                    if (cobj(data.query, old_formdata) && old_template === data.template) {
                        return;
                    } else {
                        old_formdata = data.query;
                        old_template = data.template;
                    }
                                        
                    shortcode = '[' + shortcode + ']';
                    
                    if (data.template) {
                        shortcode += data.template + '[/q_]';
                    }

                    $('#q__live-querycode > input').val(shortcode);
                    
                    if (data.template && !/^[^\[]*(\[q_.*?\]).*$/.test(data.template)) {
                        return;
                    }

                    $.post(ajaxurl, data).done(function(response) {
                        $('#q__live-results .show').slideUp(256, function() {
                            $(this).html(response).slideDown(256);
                        });
                    });
                }
                function live_refresh_on() {
                    return $('#q__live-refresh-toggle:checked').length;
                }

                function q_submit(e) {
                    if (live_refresh_on()) {
                        if (qtimeout) {
                            clearTimeout(qtimeout);
                        }
                        qtimeout = setTimeout(q_query, 1000);
                    }
                }
                
                $('#q__live-form input').keyup(q_submit);
                $('#q__live-form textarea').keyup(q_submit);
                $('#q__live-form input').mouseup(q_submit);
                $('#q__live-form select').change(q_submit);
                
                $('#q__live-refresh-toggle').off().click(function(e) {
                    if (live_refresh_on()) {
                        $('#q__live-refresh').fadeOut('fast');
                    } else {
                        $('#q__live-refresh').fadeIn('fast');
                    }
                    q_submit(e);
                });
                
                $('#q__live-refresh').off().click(q_query);
                
                $('#q__live-form .show-advanced > .show-adv-btn').off().click(function(e) {
                    e.preventDefault();
                    var adv = $('#q__live-form .advanced');
                    $(this).width($(this).width());
                    if (adv.hasClass('shown')) {
                        adv.removeClass('shown');
                        $(this).html('Show Advanced Query Options<span class="dashicons dashicons-arrow-left-alt2"></span>');
                    } else {
                        adv.addClass('shown');
                        $(this).html('Hide Advanced Query Options<span class="dashicons dashicons-arrow-down-alt2"></span>');
                    }
                    adv.slideToggle();
                });
                
                $('#q__live-querycode input').off().click(function(e) {
                    $('#q__live-querycode input').select();
                });
                
                
                function insertAtCursor(myField, myValue) {
                    //IE support
                    if (document.selection) {
                        myField.focus();
                        sel = document.selection.createRange();
                        sel.text = myValue;
                    }
                    //MOZILLA and others
                    else if (myField.selectionStart || myField.selectionStart == '0') {
                        var startPos = myField.selectionStart,
                            endPos = myField.selectionEnd;
                        myField.value = myField.value.substring(0, startPos)
                            + myValue
                            + myField.value.substring(endPos, myField.value.length);
                    } else {
                        myField.value += myValue;
                    }
                }
                
                $('.add-qcode').off().click(function(e) {
                    e.preventDefault();
                    insertAtCursor(document.getElementById('template'), $(this).text());
                    $('#template').focus();
                    q_submit();
                });
            });
        </script><?php
    }
    public function ajax()
    {
        if (!empty($_POST)) {
            $query = empty($_POST['query']) ? array() : $_POST['query'];
            $template = "<li>[q_link]</li>";
            if (!empty($_POST['template'])) {
                $template = wp_kses_post($_POST['template']);
                echo do_qquery($query, $template);
            } else {
                echo "<ol>";
                echo do_qquery($query, $template);
                echo "</ol>";
            }
        } else {
            echo '<p><strong>Error:</strong> Sorry, nothing to query? Something went wrong.</p>';
        }

        wp_die();
    }
}

new q__live();